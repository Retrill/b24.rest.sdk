<?php

namespace Glu\B24RestSdk;

use Glu\B24AppBackground\Models\Endpoint;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\TransferException;

class Client
{
    protected HttpClient $http;
    protected array $credentials = []; //array of Credentials objects
    protected array $errors = [];
    protected ?bool $authorized = null;

    public function __construct(Credentials ...$credentials)
    {
        $this->credentials = $credentials;
        $this->http = new HttpClient();
    }

    /**
     * @param array $queries List of linked queries to be performed. Each query is an array of two elements:
     *                       1) method name; 2) array of method parameters (optional)
     *                       [
     *                       'q0' => 'user.current',
     *                       'q1' => ['user.current'],
     *                       'q2' => ['department.get', ['ID' => '$result[q1][UF_DEPARTMENT]']]
     *                       'q3' => ['department.get', ['ID' => '{{ q1[UF_DEPARTMENT] }}']]
     *                       ]
     */
    public function batch(array $queries): ?array
    {
        $result = [];
        if (!\is_array($queries)) {
            return null;
        }
        $cmd = [];
        foreach ($queries as $queryId => $query) {
            $methodName = \is_scalar($query) ? $query : $query[0];
            $methodParams = \is_scalar($query) ? [] : $query[1] ?? [];
            // replacing {{ q5[key] }} to $result[q5][key]; {{ q1 }} to $result[q1]
            \array_walk_recursive($methodParams, fn (&$v) => $v = \preg_replace('#{{\s*([^\s\[]+)(\[\S+?)?\s*}}#', '\$result[$1]$2', $v));
            $cmd[$queryId] = $methodName.'?'.\http_build_query($methodParams);
        }

        return $this->exec('batch', ['cmd' => $cmd]);
    }

    /* protected function batchRequest($data, $method)
    {
        $nPageSize = 0;
        $iNumPage = 50;
        $arBatch = [];
        $queryData = \http_build_query($data);
        \parse_str(\str_replace(['__--SiZe--__', '__--PaGe--__', '__--StArT--__'], 1, $queryData), $queryData1);
        //$result = getBitrixApi($queryData1, $method); //var_dump($queryData1,$result); die();
        $result = $this->exec($method, $queryData1, $_REQUEST['auth']);
        if (!isset($result['total'],$result['result'])) {
            return false;
        }
        if ($result['total'] < 2) {
            return [$result['result']];
        }
        $nPageSize = (($result['total'] % $iNumPage) > 0 ? (intval($result['total'] / $iNumPage) + 1) : ($result['total'] / $iNumPage));
        for ($x = 0; $x++ < $nPageSize;) {
            $arBatch[] = $method.'?'.str_replace(['__--SiZe--__', '__--PaGe--__', '__--StArT--__'], [$iNumPage, $x, ($x - 1) * $iNumPage], $queryData);
        }
        $result = [];
        while (count($arBatch)) {
            //$res = getBitrixApi(['CMD'=>array_splice($arBatch,0,50)],'batch');
            $res = restCommand('batch', ['CMD' => array_splice($arBatch, 0, 50)], $_REQUEST['auth']);
            $result = array_merge($result, $res['result']['result']);
        }
        if ($nPageSize != count($result)) {
            return false;
        }

        return $result;
    } */

    public function exec(string $method, array $params = [], ?string $specificHost = null)
    {
        $results = [];
        $this->errors = [];

        foreach ($this->credentials as $credentials) {
            if ((bool) $specificHost && ($specificHost != $credentials->host)) {
                continue;
            }
            if ($credentials->authenticable) {
                $this->actualizeCredentialsByTime($credentials);
                $params = ['auth' => $credentials->auth] + $params;
            }
            $queryUrl = $credentials->endpoint.$method.'.json';
            $response = $this->post($queryUrl, $params);

            if (!empty($response['error'])) {
                $this->errors[] = '[execution error] '.$response['error'];
                if (
                    \in_array($response['error'], ['expired_token', 'invalid_token'])
                    && $this->refreshCredentials($credentials)
                ) {
                    $response = $this->{__FUNCTION__}($method, $params, $credentials->host)[$credentials->host];
                }
            }

            $results[$credentials->host] = $response;
        }

        return $results;
    }

    protected function refreshCredentials(Credentials $credentials)
    {
        // \file_put_contents(\base_path().'/dump.php', 'trying to refresh creds ', \FILE_APPEND);

        if (!isset($credentials->refresh)) {
            return false;
        }

        $queryUrl = \sprintf('https://%s/oauth/token/', $credentials->host); //oauth.bitrix.info
        $queryData = [
            'grant_type' => 'refresh_token',
            'client_id' => $credentials->appId,
            'client_secret' => $credentials->appSecret,
            'refresh_token' => $credentials->refresh,
        ];
        $response = $this->post($queryUrl, $queryData);
        // $response = \json_decode((string)$response->getBody(), true);
        // \file_put_contents(\base_path().'/dump.php', "response: ".var_export($response, true)."\n", \FILE_APPEND);
        if (!\is_array($response)) {
            $this->errors[] = 'Expired credentials can\'t be refreshed (incorrect response)!';
            return false;
        }

        if (!empty($response['error'])) {
            $this->errors[] = '[refresh token error] '.$response['error'];

            return false;
        }

        $credentials->store($response);

        return $response;
    }

    protected function actualizeCredentialsByTime(Credentials $credentials): void
    {
        if (!$credentials->authenticable) {
            return;
        }
        if (\time() > $credentials->expiresAt) {
            $this->refreshCredentials($credentials);
        }

        return;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function post(string $url, array $params)
    {
        $response = null;
        try {
            $response = $this->http->post($url, ['form_params' => $params]);
        } catch (TransferException $er) {
            $this->errors[] = '[http request error] '.$er->getMessage();
            if (\method_exists($er, 'getResponse')) {
                $response = $er->getResponse();
            }
        } catch (\Throwable $er) {
            $this->errors[] = '[http request error] '.$er->getMessage();
        }

        if (\is_null($response)) {
            return null;
        }

        $responseBody = (string) $response->getBody();
        $responseJson = \json_decode($responseBody, true);
        if (!\is_array($responseJson)) {
            $this->errors[] = 'Invalid json response by url = '.$url;
            $this->errors[] = 'First response symbols: '.\mb_substr($responseBody, 0, 500, 'UTF-8');
            return null;
        }
        return $responseJson;
    }

    public function isAuthorized()
    {
        if (!\is_null($this->authorized)) {
            return $this->authorized;
        }
        if (count($this->credentials) > 1) {
            throw new \Exception('This version of script does not support several credentials check');
        }
        $credentials = \current($this->credentials);
        if (!$credentials->host) {
            return false;
        }
        $results = $this->batch([
            'admin' => 'user.admin',
            'fingerprint' => ['app.option.get', ['option' => 'fingerprint']],
        ]);
        $results = \current($results)['result']['result'] ?? [];
        if (empty($results['admin']) || empty($results['fingerprint'])) {
            return ($this->authorized = false);
        }
        // check remote host is the same that saved in the database (but domain names may be different)
        return $this->authorized = Endpoint::where('code', '=', $credentials->code)
            ->where('fingerprint', '=', $results['fingerprint'])->exists();
    }
}
