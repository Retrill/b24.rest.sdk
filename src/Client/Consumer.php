<?php

namespace Glu\B24RestSdk\Client;

use Glu\B24RestSdk\Client as RestClient;

/**
 * This class is useful to create service container id only.
 */
class Consumer extends RestClient
{
}
