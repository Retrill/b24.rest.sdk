<?php

namespace Glu\B24RestSdk;

abstract class Credentials
{
    protected array $credentials = [];

    /* public function __construct(array $credentials = []){
        /$this->credentials = \array_change_key_case([
            'authenticable' => true,
            'host' => \preg_replace('/^https?:\/\//i', '', $endpoint->host),
            'endpoint' => $endpoint->host . $endpoint->uri,

            'auth' => $token->auth_token,
            'expiresAt' => (new \DateTime())->add(new \DateInterval('PT'.$token->lifespan.'S'))->getTimestamp();
            'refresh' => $token->refresh_token,

            'appId' => $endpoint->app_id ?: \env('APP_ID');
            'appSecret' => $endpoint->app_secret ?: \env('APP_SECRET');
        ], \CASE_LOWER);
    } */

    public function __get(string $name)
    {
        $name = \mb_strtolower($name, 'UTF-8');
        if(\array_key_exists($name, $this->credentials)){
            return $this->credentials[$name];
        }else{
            throw new \Exception("\"$name\" property does not exist, 'get' mode (from ".(new \ReflectionClass($this))->getName().") " . $this);
        }
    }

    public function __isset( string $name ): bool
    {

        return \array_key_exists(\mb_strtolower($name, 'UTF-8'), $this->credentials);
    }

    abstract public function store(array $rawCredentials);

    // abstract public function restore(?array $endpointFilter, ?array $tokenFilter);
}
