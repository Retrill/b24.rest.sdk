<?php

namespace Glu\B24RestSdk\Credentials;

use Glu\B24RestSdk\Credentials;

// use Glu\B24AppBackground\Models\{Endpoint, Consumer\Token};
// use Glu\B24AppBackground\Db\Factories\Consumer\TokenFactory;

class ArrayBased extends Credentials
{
    public function store(array $rawCredentials): Credentials
    {
        $this->makeInstance($rawCredentials);
        // do nothing
        return $this;
    }

    protected function makeInstance(array $credentials): Credentials
    {
        // $host = \rtrim($request->input('DOMAIN', $request->input('domain')), '/');
        $host = $credentials['DOMAIN'] ?? $credentials['domain'] ?? '';
        $host = \preg_replace('/^https?:\/\//i', '', $host);

        $this->credentials = \array_change_key_case([
            'authenticable' => true,
            'host' => $host,
            'endpoint' => $host ? 'https://'.$host.'/rest/' : '',
            'code' => $credentials['member_id'] ?? '',
            'auth' => $credentials['AUTH_ID'] ?? $credentials['access_token'] ?? '',
            'expiresAt' => $credentials['expires'] ?? (isset($credentials['AUTH_EXPIRES']) ? (\time() + (int) $credentials['AUTH_EXPIRES']) : ''),
            'refresh' => $credentials['REFRESH_ID'] ?? $credentials['refresh_token'] ?? '',
            'appId' => $credentials['app_id'] ?? \env('APP_ID', ''),
            'appSecret' => $credentials['app_secret'] ?? \env('APP_SECRET', ''),
        ], \CASE_LOWER);

        return $this;
    }

    public static function __callStatic(string $name, array $arguments)
    {
        $name = \strtolower($name);
        if ('makeinstance' != $name) {
            throw new \Exception('Unexpected method name');
        }

        return (new static())->{$name}(...$arguments);
    }
}
