<?php

namespace Glu\B24RestSdk\Credentials;

class FileBased implements \Glu\B24RestSdk\Credentials
{
    protected $credentialsDir = '';

    public function __construct()
    {
        $this->credentialsDir = __DIR__.'/credentials/';
    }

    public function setCredentialsDir(string $path)
    {
        $this->credentialsDir = \rtrim($path, '/').'/';
    }

    public function store(array $credentials)
    {
    }

    public function index(?array $filter)
    {
    }
}
