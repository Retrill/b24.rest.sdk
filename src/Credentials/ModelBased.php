<?php

namespace Glu\B24RestSdk\Credentials;

use Glu\B24RestSdk\Credentials;
use Glu\B24AppBackground\Models\{Endpoint, Consumer\Token};
use Glu\B24AppBackground\Db\Factories\Consumer\TokenFactory;

class ModelBased extends Credentials
{
    protected ?Endpoint $endpoint;
    protected ?Token $token;

    public function store(array $rawCredentials): Credentials
    {
        // Пока что считаем, что endpoint в обновлении не нуждается (только токен)
        if(empty($this->token)){
            throw new \Exception('Can\'t store uninitialized credentials!');
        }

        // $newTokenAttrs = TokenFactory::canonicalizeAttrs($rawCredentials);
        // \array_walk($newTokenAttrs, fn($v, $propName) => $this->token->{$propName} = $v);
        $this->token->auth_token = $rawCredentials['access_token'] ?? $rawCredentials['AUTH_ID'];
        // $this->token->expiresAt = \DateTime::createFromFormat('U', $rawCredentials['expires'])->setTimezone(new \DateTimeZone(\date('T')))->format('Y-m-d H:i:s'); // timestamp => formatted datetime
        $this->token->valid_through = $rawCredentials['expires'] ?? (\time() + $rawCredentials['AUTH_EXPIRES']);
        $this->token->refresh_token = $rawCredentials['refresh_token'] ?? $rawCredentials['REFRESH_ID'];
        $this->token->lang = $rawCredentials['LANG'] ?? $this->token->lang;
        $this->token->bx_user_id = $rawCredentials['user_id'] ?? $this->token->bx_user_id;
        $this->token->save();
        $this->makeInstance($this->endpoint, $this->token);
        return $this;
    }

    protected function makeInstance(Endpoint $endpoint, Token $token): Credentials{
        $this->endpoint = $endpoint;
        $this->token = $token;

        $this->credentials = \array_change_key_case([
            'authenticable' => $endpoint->authenticable,
            'host' => \preg_replace('/^https?:\/\//i', '', $endpoint->host),
            'endpoint' => 'https://'.$endpoint->host.$endpoint->uri,
            'code' => $endpoint->code,
            'auth' => $token->auth_token,
            // 'lifespan' => $token->lifespan,
            // 'expiresAt' => (new \DateTime())->add(new \DateInterval('PT'.$token->lifespan.'S'))->getTimestamp();
            // 'expiresAt' => (new \DateTime())->add(new \DateInterval('PT'.$token->lifespan.'S'))->getTimestamp();
            'expiresAt' => $token->valid_through->timestamp,
            'refresh' => $token->refresh_token,
            'appId' => $endpoint->app_id ?: \env('APP_ID'),
            'appSecret' => $endpoint->app_secret ?: \env('APP_SECRET')
        ], \CASE_LOWER);
        // \dump('credentials:', $this->credentials);

        return $this;
    }

    public static function __callStatic(string $name, array $arguments){
        $name = \strtolower($name);
        if($name != 'makeinstance'){
            throw new \Exception('Unexpected method name');
        }

        return (new static())->{$name}(...$arguments);
    }
}
