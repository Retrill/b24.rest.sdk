<?php

namespace Glu\B24RestSdk\Tests\Unit;

// use Glu\B24RestSdk\Models\Endpoint;
use Glu\B24AppBackground\Tests\Traits\Credentialable;
use Glu\B24AppBackground\Models\{Endpoint, Consumer\Token};
use Glu\B24RestSdk\Client;
use Glu\B24RestSdk\Credentials\ModelBased as Credentials;
use Glu\B24RestSdk\Credentials\ArrayBased as ArrayCredentials;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class ClientTest extends TestCase
{
    use DatabaseMigrations, Credentialable;

    public function testAuthTokenMayBeRefreshed(){
        if(!$host = \trim(\env('TESTB24_HOST', ''))){
            $this->markTestIncomplete('Test bitrix24 host is not set! Test will be ignored');
        }

        $info = $this->install([
            'app_id' => \env('TESTB24_APPID'),
            'app_secret' => \env('TESTB24_APPSECRET'),
        ]);
        $token = $info['token'];
        // after installation we have already prolonged auth-token, so rollback it:
        $token->auth_token = \env('TESTB24_AUTH_TOKEN');
        $token->refresh_token = \env('TESTB24_REFRESH_TOKEN');

        // Emulate token to be outdated by "valid through":
        $token->valid_through = $token->valid_through->addSeconds(-3600*24);
        $outdatedToken = $token->replicate();
        $credentials = Credentials::makeInstance($info['endpoint'], $token);

        $restClient = new Client($credentials);
        $result = $restClient->exec('app.info');

        $token->refresh();
        $this->assertNotEmpty($token->auth_token, 'New auth token should not be empty');
        $this->assertNotEquals($token->auth_token, $outdatedToken->auth_token, 'Outdated token should be updated');
        $this->assertNotEquals($token->refresh_token, $outdatedToken->refresh_token, 'Outdated refresh token should be updated');
        $this->assertEquals($token->system, $outdatedToken->system, '"System" flag should be the same after token prolongation');
        $this->assertGreaterThan(0, $token->bx_user_id, 'Bitrix user id should be assigned during token prolongation');
        $this->assertGreaterThan(time(), $token->valid_through->timestamp, '"Valid through" should be a moment in future');
    }

    public function testAuthTokenShouldNotBeRefreshedTwice(){
        if(!$host = \trim(\env('TESTB24_HOST', ''))){
            $this->markTestIncomplete('Test bitrix24 host is not set! Test will be ignored');
        }

        $info = $this->install([
            'app_id' => \env('TESTB24_APPID'),
            'app_secret' => \env('TESTB24_APPSECRET'),
        ]);
        $token = $info['token'];
        // after installation we have already prolonged auth-token, so rollback it:
        $token->auth_token = \env('TESTB24_AUTH_TOKEN');
        $token->refresh_token = \env('TESTB24_REFRESH_TOKEN');

        $token->valid_through = $token->valid_through->addSeconds(-3600*24);
        $credentials = Credentials::makeInstance($info['endpoint'], $token);
        $restClient = new Client($credentials);
        $restClient->exec('app.info');
        $firstlyRefreshed = $token->replicate();
        $restClient->exec('app.info');
        $this->assertEquals($token->auth_token, $firstlyRefreshed->auth_token, 'Auth token should not be updated twice');
    }

    public function testFingerprintShouldBeSaved(){
        if(!$host = \trim(\env('TESTB24_HOST', ''))){
            $this->markTestIncomplete('Test bitrix24 host is not set! Test will be ignored');
        }
        $i = $this->install([
            'app_id' => \env('TESTB24_APPID'),
            'app_secret' => \env('TESTB24_APPSECRET'),
        ]);
        $this->assertNotEmpty($i['endpoint']->fingerprint, 'Installed fingerprint should not be empty');
        $restClient = new Client(Credentials::makeInstance($i['endpoint'], $i['token']));
        $response = $restClient->exec('app.option.get', ['option' => 'fingerprint']);
        $savedFingerprint = \current($response)['result'];
        $this->assertEquals($savedFingerprint, $i['endpoint']->fingerprint);
    }

    public function testBatchQuery(){
        if(!$host = \trim(\env('TESTB24_HOST', ''))){
            $this->markTestIncomplete('Test bitrix24 host is not set! Test will be ignored');
        }
        $credentials = ArrayCredentials::makeInstance([
            'DOMAIN' => \env('TESTB24_HOST'),
            'AUTH_ID' => \env('TESTB24_AUTH_TOKEN'),
            'REFRESH_ID' => \env('TESTB24_REFRESH_TOKEN'),
            'app_id' => \env('TESTB24_APPID'),
            'app_secret' => \env('TESTB24_APPSECRET'),
        ] + $this->generateB24Credentials());
        $restClient = new Client($credentials);
        $testValue = Str::randomDict(128, 'A-Z a-z \d');
        $result = $restClient->batch([
            'q1' => ['app.option.set', ['options' => ['myTestOpt' => $testValue]]],
            'q2' => ['app.option.get', ['option' => 'myTestOpt']],
            'q3' => ['app.option.set', ['options' => ['myTestOpt2' => '{{q2}}']]],
            'q4' => ['app.option.get', ['option' => 'myTestOpt2']],
            'q5' => 'app.option.get',
            'q6' => ['app.option.set', ['options' => ['myTestOpt2' => '$result[q5][myTestOpt] 2']]],
            'q7' => ['app.option.get', ['option' => 'myTestOpt2']],
        ]);
        $queryResults = \current($result)['result']['result'];
        $this->assertEquals($queryResults['q4'], $testValue);
        $this->assertEquals($queryResults['q7'], "$testValue 2");
    }

    protected function install(?array $rawCredentials = []): array
    {
        $rawCredentials = $rawCredentials + [
            'DOMAIN' => \env('TESTB24_HOST'),
            'AUTH_ID' => \env('TESTB24_AUTH_TOKEN'),
            'REFRESH_ID' => \env('TESTB24_REFRESH_TOKEN'),
        ] + $this->generateB24Credentials();

        $this->post(\route('install', 1), $rawCredentials);

        $endpoint = Endpoint::first();
        // $endpoint->app_id = \env('TESTB24_APPID');
        // $endpoint->app_secret = \env('TESTB24_APPSECRET');
        return [
            'endpoint' => $endpoint,
            'token' => Token::first()
        ];
    }
}
